/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React,{useEffect} from 'react';
import
  {
    StyleSheet,
  } from 'react-native';
import Home from './src/screens/home';
import SplashScreen from 'react-native-splash-screen'




function App()
{
  
  return (
   <Home/>
  );
}

const styles = StyleSheet.create({});

export default App;
