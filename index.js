/**
 * @format
 */

 import React from "react";
 //wix react-native navigation
 import {
   Navigation
 } from "react-native-navigation";
 import {
  PageNavigator
 } from "./src/provider/page-navigator";
 import withPaperProvider from "./src/hoc/withPaperProvider.hoc";

Navigation.registerComponent('Home', () => withPaperProvider(require('./src/screens/home').default));
Navigation.registerComponent('Splash', () => withPaperProvider(require('./src/screens/splashScreen').default));
Navigation.registerComponent('SignIn', () => require('./src/screens/home').default);

Navigation.events().registerAppLaunchedListener(() => {
  PageNavigator.setRoot("Home");
});