import React from 'react';
import { Provider } from 'react-native-paper';
import theme from '../theme';
const withPaperProvider = (WrappedComponent) => {
    class HOC extends React.Component {
        render() {
            return ( 
                <Provider theme={theme} >
            <WrappedComponent {...this.props} />
        </Provider>
            );
        }
    }

    return HOC;
};

export default withPaperProvider;