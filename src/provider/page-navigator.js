import { Navigation } from "react-native-navigation";

export class PageNavigator {

  //setg root
  static setRoot(componentName, stackOptions, props) {
    stackOptions ? stackOptions : {};
    let stack = {
      root: {
        stack: {
          // sideMenu: {
          //   left: {
          //     component: {
          //       name: 'DrawerContent',
          //       passProps: props || {}
          //     }
          // },
          children: [
            {
              component: {
                name: componentName,
                passProps: props || {},
                options: {
                  ...{
                    topBar: {
                      visible: false
                    }
                  },
                  ...this.statusBarOptions,
                  ...stackOptions,
                  bottomTabs: {
                    visible: false,
                    animate: true,
                    drawBehind: true
                  }
                }
              }
            }
          ]
        }
      }
      // }

    };
    return Navigation.setRoot(stack);
  }

  //Home page

  static ViewHomePage(props, componentName,) {
    let stackData = {
      root: {
        id: 'rootId',
        sideMenu: {
          left: {
            component: {
              name: 'CustomDrawer',
              passProps: {
                componentId: props.componentId,
                componentName: componentName
              }
            }
          },
          center: {
            stack: {
              options: {
                topBar: {
                  visible: false
                }
              },
              children: [
                {
                  component: {
                    name: componentName,
                    passProps: props || {}
                  }
                }
              ]
            }
          }

        }


      }
    }
    return Navigation.setRoot(stackData);
  }

//View sign
  static viewSignIn(props) {
    Navigation.push(props.componentId, {
      component: {
        name: 'SignIn',
        passProps: props || {},
        options: {
          topBar: {
            visible: false,
            title: {
              text: 'Login',
            },

          },
        },
      },
    });
  }
}