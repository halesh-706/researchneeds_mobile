import React, {useEffect, useState} from 'react';
import {
  Appbar,
  Avatar,
  Divider,
  ActivityIndicator,
  Surface,
} from 'react-native-paper';

import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  StatusBar,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';

import {Navigation} from 'react-native-navigation';

function Dashboard(props) {
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    setTimeout(function () {
      setLoading(false);
    }, 5000);
  }, []);
  return (
    <View style={{flex: 1}}>
      <Appbar.Header >
        <Appbar.Action
          icon="arrow-left"
          onPress={() => {
            Navigation.pop(props.componentId);
          }}
        />
        <Appbar.Content
          style={{marginHorizontal: 25}}
          title={'Dashboard'}
        />
      </Appbar.Header>
      <View
        style={{
          flex: 0.25,
          marginHorizontal: 30,
          marginVertical: 10,
        }}>
        {!isLoading && (
          <Image
            style={{width: '100%', height: 60}}
            source={{
              uri:
                'http://researchneeds.in/paas/static_files/uploads/app/site/RNLOGO.png',
            }}
          />
        )}
      </View>
      <View style={{flex: 0.3}}>
        {isLoading && (
          <ActivityIndicator
            
            animating={true}
            size="small"
          />
        )}
        {!isLoading && (
          <Surface style={styles.surface}>
            <Text
              style={{
                textAlign: 'center',
                color: '#f5821f',
                fontSize: 20,
                fontWeight: 'bold',
              }}>
              Welcome to research need{' '}
            </Text>
          </Surface>
        )}
      </View>
      {!isLoading && (
        <View style={{flex: 0.45}}>
          <Text style={{fontSize: 50, textAlign: 'center'}}>🎉</Text>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20,
  },
  surface: {
    padding: 8,
    width: '100%',
    justifyContent: 'center',
    elevation: 4,
  },
});

export default Dashboard;
